const kafka = require('./kafka/producer');
let payloads = [];
function makeString(length) {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}
for (let index = 0; index < 10000; index++) { // Producer should produce 10000 JSON messages.
    dataToPush = JSON.stringify({ massage: makeString(5), no: index });
    var payload = kafka.payload;
    payloads.push({  topic: payload.topic,partitions: payload.partitions, timestamp: Date.now(), messages: dataToPush });
}
kafka.kafkaProducer(payloads);