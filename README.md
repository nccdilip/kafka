# README #

This README would normally document whatever steps are necessary to get your application up and running.


### How do I get set up? ###

* Do NPM install
* add mongodb details in config.js
* install kafka
* Set the three cluster node ( Follow - Step to Setup a Kafka cluster with three nodes.docx )
* Start Producer ( node startProducer.js )
* Start Consumer ( node startConsumer.js )
* Stop Consumer ( node stopConsumer.js )