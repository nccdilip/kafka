const config = require('../config');
var Messages = require('../model/messages');
kafkaconsumer = () => {
    const kafka = require('kafka-node'),
    Consumer = kafka.Consumer,
    client = new kafka.KafkaClient(),

    consumer = new Consumer(
        client,
        [{
            topic: config.kafka_topic,
            partition: 0
        }], {
        fromOffset: false, //When the consumer is started again, it should start from the next message in the queue.
        autoCommit: false,
    });

    consumer.on('message', function (message) {
        var msg = JSON.parse(message.value)
        console.log("errormsg", typeof (msg));
        if (typeof (msg) != 'object') {
            consumer.commit(true, function (err, data) {
                console.log(err, data)

                consumer.close(true, function (err) {
                    console.log("consumer has been closed..", err);
                });

            });

        } else {
            data = {
                messagePayload: msg
            };
            //Consumer should log the messages to Mongo DB and a file.
            var messages = Messages(data);
            messages.save(function (err) {
                if (err) {
                    console.log(err)
                }
                setTimeout(()=>{
                    consumer.commit(true, function (err, data) {
                        console.log(err, data);
                    });
                }, 1000); //wait for 1s. 
            });
        }
    });
}
exports.kafkaConsumer = kafkaconsumer
