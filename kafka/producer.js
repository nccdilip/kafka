const config = require('../config');
let payload =  { topic: config.kafka_topic, partitions:0};
kafka_producer = (payloads) => {
    var kafka = require('kafka-node'),
    Producer = kafka.Producer,
    client = new kafka.KafkaClient(config.kafka_server),
    producer = new Producer(client);
    producer.on('ready', function () {
        producer.send(payloads, function (err, data) {
            console.log(data);
            producer.close();
        });

    });
    producer.on('error', function (err) {
        console.log(err)
    })
}
exports.payload = payload
exports.kafkaProducer = kafka_producer
