const kafka = require('./kafka/producer');
let payloads = [];
var payload = kafka.payload;
function makeString(length) {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}
payloads.push({ topic: payload.topic,partitions: payload.partitions,timestamp: Date.now(), messages: makeString(6)}); // Producer should randomly produce non JSON messages. Consumer should exit when the messages are not in JSON format.
kafka.kafkaProducer(payloads);