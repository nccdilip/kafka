'use strict';

var mongoose = require('mongoose');
const config = require('../config');
mongoose.connect(config.database);
const Schema = mongoose.Schema;

const MessageSchema = new Schema({
    messagePayload: Schema.Types.Mixed
},{timestamps: true});
module.exports = mongoose.model('Message', MessageSchema);